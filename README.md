# AIOHTTP Proxy
AIOHTTP Proxy is an application that proxies requests for [lifehacker.ru](https://lifehacker.ru) and adds emoji to each
 word with a length of 6 characters 😎.

## Requirements
Please note: the code has not been tested on other versions of dependencies.
* [Python 3.7](https://www.python.org/downloads/release/python-370/)
* [aiohttp 3.6.2](https://github.com/aio-libs/aiohttp/releases/tag/v3.6.2)

A complete list of dependencies is available in requiremets.txt

## Installation
```bash
$ pip install -r requirements.txt
```

## Usage
```bash
$ python server.py
```

## License
MIT
